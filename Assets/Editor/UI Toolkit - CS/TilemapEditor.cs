using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System.Collections.Generic;

public class TilemapEditor : EditorWindow
{
    public enum State
    {
        Draw,
        Erase
    };

    Texture currentTile; // Texture of currently selected tile
    State drawState; // Draw/Erase

    Texture newTileTexture;
    string newTileName;

    List<Tile> tiles;
    ListView tileSelection;

    Texture defaultTexture;

    [MenuItem("Tools/Tilemap Editor")]
    public static void ShowExample()
    {
        TilemapEditor wnd = GetWindow<TilemapEditor>();
        wnd.titleContent = new GUIContent("TilemapEditor");
    }

    private void SetVisualElementBorder(VisualElement element, float width, Color color)
    {
        element.style.borderTopWidth = element.style.borderBottomWidth =
            element.style.borderLeftWidth = element.style.borderRightWidth = width;
        element.style.borderTopColor = element.style.borderBottomColor =
            element.style.borderLeftColor = element.style.borderRightColor = color;
    }


    public void CreateGUI()
    {
        defaultTexture = Resources.Load<Texture>("Taco");
        currentTile = defaultTexture;

        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        VisualElement mainPanel = new VisualElement();
        mainPanel.style.flexGrow = 1.0f;
        mainPanel.style.flexDirection = FlexDirection.Row;
        SetVisualElementBorder(mainPanel, 1.0f, Color.black);
        root.Add(mainPanel);

        #region Left Panel
        VisualElement leftPanel = new VisualElement();
        leftPanel.style.flexGrow = 0.33f;
        leftPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(leftPanel, 1.0f, Color.black);
        mainPanel.Add(leftPanel);

        #region Tile Creation
        VisualElement tileCreationPanel = new VisualElement();
        tileCreationPanel.style.flexGrow = 0.2f;
        tileCreationPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(tileCreationPanel, 1.0f, Color.black);
        tileCreationPanel.Add(new Label("Tile Creation"));
        leftPanel.Add(tileCreationPanel);

        ObjectField tileObjectField = new ObjectField("Texture: ");
        tileObjectField.objectType = typeof(Texture);
        tileObjectField.RegisterCallback<ChangeEvent<Object>>(OnTextureChanged);
        tileCreationPanel.Add(tileObjectField);

        TextField newTileNameTextField = new TextField("Name: ");
        newTileNameTextField.RegisterCallback<ChangeEvent<string>>(OnNameChanged);
        tileCreationPanel.Add(newTileNameTextField);

        Button createTileButton = new Button(CreateNewTile)
        {
            text = "Create New Tile",
            style =
            {
                width = 100,
                height = 30
            }
        };
        tileCreationPanel.Add(createTileButton);

        #endregion

        #region Tile Selection

        VisualElement tileSelectionPanel = new VisualElement();
        tileSelectionPanel.style.flexGrow = 0.8f;
        tileSelectionPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(tileSelectionPanel, 1.0f, Color.black);
        leftPanel.Add(tileSelectionPanel);

        EnumField drawingState = new EnumField("Drawing type: ", State.Draw);
        drawingState.RegisterValueChangedCallback<System.Enum>(
            (_evt) =>
            {
               drawState = (State)_evt.newValue;
            }
        );
        tileSelectionPanel.Add(drawingState);

        tiles = new List<Tile>(Resources.LoadAll<Tile>("ScrObjects/Tiles/"));

        tileSelectionPanel.Add(new Label("Select Tile"));

        tileSelection = new ListView(tiles);
        tileSelection.selectionType = SelectionType.Single;
        tileSelection.style.flexGrow = 1.0f;
        tileSelection.onSelectionChange += x => currentTile = (tileSelection.selectedItem as Tile).tileImage;

        tileSelectionPanel.Add(tileSelection);
        #endregion

        #endregion

        #region Tilemap Panel
        VisualElement tilemapPanel = new VisualElement();
        tilemapPanel.style.flexGrow = 0.34f;
        tilemapPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(tilemapPanel, 1.0f, Color.black);
        mainPanel.Add(tilemapPanel);


        // Map layout
        for (int y = 0; y < 10; y++)
        {
            VisualElement rowPanel = new VisualElement();
            rowPanel.style.flexDirection = FlexDirection.Row;
            tilemapPanel.Add(rowPanel);

            for(int x = 0; x < 10; x++)
            {
                Box box = new Box()
                {
                    style =
                    {
                        minWidth = 64,
                        minHeight = 64,
                        maxWidth = 64,
                        maxHeight = 64
                    }
                };
                
                SetVisualElementBorder(box, 1, Color.black);

                box.RegisterCallback<MouseUpEvent>(OnMouseUpEvent);

                Image image = new Image();
                image.style.flexShrink = 1.0f;
                image.image = defaultTexture;
                box.Add(image);

                rowPanel.Add(box);
            }
        }
        #endregion
    }
    private void OnTextureChanged(ChangeEvent<Object> _evt)
    {
        newTileTexture = _evt.newValue as Texture; 
    }

    private void OnNameChanged(ChangeEvent<string> _evt)
    {
        newTileName = _evt.newValue;
    }

    private void OnMouseUpEvent(MouseUpEvent _event)
    {
        Image image = _event.target as Image;

        switch(drawState)
        {
            case State.Draw:
                if (image != null)
                    image.image = currentTile;
                break;

            case State.Erase:
                if (image != null)
                    image.image = defaultTexture;
                break;

            default:
                break;
        }
    }

    private void CreateNewTile()
    {
        Tile tile = CreateInstance<Tile>();

        if (newTileName != "")
            tile.name = newTileName;
        else
            tile.name = "defaultName";

        if (newTileTexture != null)
            tile.tileImage = newTileTexture;
        else
            tile.tileImage = defaultTexture;

        string path = $"Assets/Resources/ScrObjects/Tiles/{tile.name}.asset";

        try
        {
            AssetDatabase.CreateAsset(tile, path);
        }
        catch (System.Exception e)
        {
            EditorUtility.DisplayDialog("Error", "Error creating new asset. Maybe it's already exists?", "OK");
            return;
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        tiles.Add(tile);
        tileSelection.Rebuild();
    }
}