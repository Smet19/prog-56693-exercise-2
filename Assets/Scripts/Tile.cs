using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tile", menuName = "Editor Data/Tile")]
public class Tile : ScriptableObject
{
    public string tileName;
    public Texture tileImage;
}
